<?php
/**
 * The header for our theme
 *
 * @package Appica 2
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php appica_favicon(); ?>
	<?php wp_head(); ?>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61067048-2', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body <?php body_class( appica_body_extra_class() ); ?>>

	<?php appica_preloader(); ?>

	<div class="fake-scrollbar"></div>

	<?php get_sidebar( 'off-canvas' ); ?>

	<?php appica_subscribe_modal_form(); ?>

	<?php
	/*
	 * Intro section
	 *
	 * If intro is enabled also open div.content-wrap before <header>
	 * and close it before <footer>
	 */
	if ( appica_is_intro() ) :

		// Check the type of intro screen
		if ( 'revslider' === appica_intro_type() ) : ?>

			<section class="intro-slider">
				<div class="container">
					<div class="column-wrap">
						<div class="column c-left">
							<?php // appica_intro_socials(); ?>
						</div>
						<div class="column c-middle"></div>
						<div class="column c-right">
							<div class="navi">
								<?php appica_intro_subscribe(); ?>
								<div class="nav-toggle" data-offcanvas="open"><i class="flaticon-list26"></i></div>
							</div>
						</div>
					</div>
				</div>
			</section>

	<div id="slider-desktop">
			<?php putRevSlider("slider-parallax") ?>
    </div>
    
    <div id="slider-mobile">
			<?php putRevSlider("slider2") ?>
    </div>

		<?php else: ?>

			<section class="intro" <?php appica_intro_background(); ?>>

				<?php if ( appica_is_intro_gradient() ) : ?>
					<div class="gradient"></div>
				<?php endif; ?>

				<div class="container">
					<div class="column-wrap">
						<div class="column c-left">
							<?php //appica_intro_socials(); ?>
							<?php appica_intro_scroll(); ?>
						</div>

						<div class="column c-middle">
							<?php appica_intro_logo(); ?>
							<?php appica_intro_screen(); ?>
						</div>

						<div class="column c-right">
							<div class="navi">
								<?php appica_intro_subscribe(); ?>
								<div class="nav-toggle" data-offcanvas="open"><i class="flaticon-list26"></i></div>
							</div>

							<?php appica_intro_features(); ?>
							<?php appica_intro_download(); ?>
						</div>
					</div>
				</div>
			</section>

		<?php endif; // end check intro type ?>

	<div class="content-wrap">
	<?php endif; // end check is_intro ?>
    
    
    <?php if ( is_page_template( 'page-blank.php' ) ) { ?>
	 
<?php } else { ?>
	

	<header class="<?php appica_the_sticky_navbar(); ?>">
		<div class="container">

			<?php appica_navbar_logo(); ?>
			

			<div class="toolbar">
				<?php appica_navbar_download_button(); ?>
				<?php appica_navbar_subscribe(); ?>
				<div class="nav-toggle" data-offcanvas="open"><i class="flaticon-list26"></i></div>
			</div>

		</div>
	</header>
<?php  } ?>