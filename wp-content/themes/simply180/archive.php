<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Appica
 */

get_header(); ?>


<div class="page-heading  <?php appica_is_page_heading( $appica_title, true ); ?>">
	<div class="container">
    
   <div class="row">
				
				<div class="col-md-7" style="text-align:left">
                <?php
					
				$cat_name = single_cat_title( '', false );
				single_cat_title( '<h2>Blog: ', '</h2>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>

				</div>
				<div class="col-md-5 text-right">
                
    <form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">

		<?php
		$args = array(
			'show_option_none' => __( 'All Categories' ),
			'show_count'       => 0,
			'orderby'          => 'name',
			'echo'             => 0,
			'hide_empty'         => 0, 
		);
		?>

		<?php $select  = wp_dropdown_categories( $args ); ?>
		<?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
		<?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>

		<?php echo $select; ?>

		<noscript>
			<input type="submit" value="View" />
		</noscript>

	</form>
					
				</div>
	</div>
                
		
	</div>
</div>


<section class="space-top padding-bottom">
	<div class="container">
    

		<?php if ( have_posts() ) : ?>
              

			<div class="masonry-grid">
				<div class="grid-sizer"></div>
				<div class="gutter-sizer"></div>

				<?php
				while ( have_posts() ) :
					the_post();

					get_template_part( 'content' );

				endwhile;
				?>

			</div>

			<?php appica_paginate_links(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>


	</div>
</section>

<?php get_footer(); ?>
